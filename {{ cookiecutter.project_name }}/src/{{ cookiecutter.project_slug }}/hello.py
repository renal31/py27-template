"""Hello module."""


def hello():
    # type: () -> None
    """Print hello world."""
    print("hello world")
