"""Tasks for maintaining the project.

Execute 'invoke --list' for guidance on using Invoke
"""
import os
import platform
import webbrowser
from pathlib import Path

from invoke import task
from invoke.context import Context
from invoke.runners import Result

# from invoke import call

ROOT_DIR = Path(__file__).parent
DOCS_DIR = ROOT_DIR.joinpath("docs")
DOCS_BUILD_DIR = DOCS_DIR.joinpath("_build")
DOCS_INDEX = DOCS_BUILD_DIR.joinpath("index.html")
TEST_REPORTS_DIR = ROOT_DIR.joinpath("reports")
COVERAGE_FILE = ROOT_DIR.joinpath(".coverage")
COVERAGE_DIR = TEST_REPORTS_DIR.joinpath("htmlcov")
COVERAGE_REPORT = COVERAGE_DIR.joinpath("index.html")
SOURCE_DIR = ROOT_DIR.joinpath("src")
TEST_DIR = ROOT_DIR.joinpath("tests")
PYTHON_TARGETS = [
    SOURCE_DIR,
    TEST_DIR,
    Path(__file__),
]
PYTHON_TARGETS_STR = " ".join([str(p) for p in PYTHON_TARGETS])


def _run(c, command):
    # type: (Context, str) -> Result
    os.environ["PYTHON_TARGETS"] = PYTHON_TARGETS_STR
    return c.run(command, pty=platform.system() != "Windows")


@task()
def clean_build(c):
    # type: (Context) -> None
    """Clean up files from package building.

    Args:
        c (Context): execution context
    """
    _run(c, "rm -fr build/")
    _run(c, "rm -fr dist/")
    _run(c, "rm -fr .eggs/")
    _run(c, "find . -name '*.egg-info' -exec rm -fr {} +")
    _run(c, "find . -name '*.egg' -exec rm -f {} +")


@task()
def clean_python(c):
    # type: (Context) -> None
    """Clean up python file artifacts.

    Args:
        c (Context): execution context
    """
    _run(c, "find . -name '*.pyc' -exec rm -f {} +")
    _run(c, "find . -name '*.pyo' -exec rm -f {} +")
    _run(c, "find . -name '*~' -exec rm -f {} +")
    _run(c, "find . -name '__pycache__' -exec rm -fr {} +")


@task()
def clean_tests(c):
    # type: (Context) -> None
    """Clean up files from testing.

    Args:
        c (Context): execution context
    """
    _run(c, "rm -f %s" % COVERAGE_FILE)
    _run(c, "rm -fr %s" % TEST_REPORTS_DIR)
    _run(c, "rm -fr .pytest_cache")


@task()
def clean_docs(c):
    # type: (Context) -> None
    """Clean up files from documentation builds.

    Args:
        c (Context): execution context
    """
    _run(c, "rm -fr %s" % DOCS_BUILD_DIR)


@task(pre=[clean_build, clean_python, clean_tests, clean_docs])
def clean(c):
    # type: (Context) -> None
    """Run all clean sub-tasks.

    Args:
        c (Context): execution context
    """


@task()
def install_hooks(c):
    # type: (Context) -> None
    """Install pre-commit hooks.

    Args:
        c (Context): execution context
    """
    _run(c, "git init")
    _run(c, "tox -e py3-install")


@task(pre=[install_hooks])
def install(c):
    # type: (Context) -> None
    """Run all install sub-tasks.

    Args:
        c (Context): execution context
    """


@task()
def hooks(c):
    # type: (Context) -> None
    """Run pre-commit hooks.

    Args:
        c (Context): execution context
    """
    _run(c, "tox -e py3-hooks")


@task(name="format", help={"check": "Checks if source is formatted without applying changes"})
def format_(c, check=False):
    # type: (Context, bool) -> None
    """Format code.

    Args:
        c (Context): execution context
        check (bool, optional): check only (disable auto formatting). Defaults to False.
    """
    if check:
        _run(c, "tox -e py3-format-check")
    else:
        _run(c, "tox -e py3-format")


@task()
def commit_check(c):
    # type: (Context) -> None
    """Check commit messages format.

    Args:
        c (Context): execution context
    """
    _run(c, "tox -e py3-commit-check")


@task()
def lint(c):
    # type: (Context) -> None
    """Run flake8.

    Args:
        c (Context): execution context
    """
    _run(c, "tox -e py3-lint")


@task()
def commit(c):
    # type: (Context) -> None
    """Run commitizen.

    Args:
        c (Context): execution context
    """
    _run(c, "tox -e py3-commit")


@task()
def release(c):
    # type: (Context) -> None
    """Release package: bump version, generate changelog and set git tag.

    Args:
        c (Context): execution context
    """
    _run(c, "tox -e py3-release")


@task(name="test", help={"coverage": "Generate tests code coverage"})
def test(c, coverage=False, open_browser=False):
    # type: (Context, bool, bool) -> None
    """Run tests.

    Args:
        c (Context): execution context
        coverage (bool, optional): check only (disable auto formatting). Defaults to False.
        open_browser (bool, optional): open coverage html reports in browser. Defaults to False.
    """
    options = ""
    if coverage:
        options = " --with-coverage --coverage-report xml --coverage-report html"
    _run(c, "mkdir -p reports")
    _run(c, "nose2 %s" % options)
    if coverage and open_browser:
        webbrowser.open(COVERAGE_REPORT.as_uri())
