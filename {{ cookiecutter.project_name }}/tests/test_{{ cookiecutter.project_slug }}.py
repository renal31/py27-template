from mock import patch
from StringIO import StringIO

from {{ cookiecutter.project_slug }} import hello


@patch("sys.stdout", new_callable=StringIO)
def test_hello(mock_stdout):
    hello.hello()
    assert mock_stdout.getvalue() == "hello world\n"
