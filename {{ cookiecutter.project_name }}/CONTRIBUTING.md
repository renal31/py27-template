# Contributing

## Pre-requisite

- [Install python 3.x](https://www.python.org/downloads/)
- [Install poetry](https://python-poetry.org/docs/#installation) dependency management tool
- Set following configuration:

```bash
poetry config virtualenvs.create true
poetry config virtualenvs.in-project true
```

Links:

- [Poetry commands](https://python-poetry.org/docs/cli/)
- [Adding private repositories](https://python-poetry.org/docs/repositories/#using-a-private-repository)

## Get started

- Clone the repository
- Install dependencies as follow:

```bash
cd <project root>
poetry install
poetry run invoke install
```

### Troubleshooting

If you get following error message when running `poetry install`:

> OSError
>
> Unable to access any of "\<project-path\>/.venv/local/lib/python2.7/dist-packages/\<project-pack\>"

Try running this to fix it:

```bash
mkdir -p $(poetry env info -p)/local/lib/python2.7/dist-packages
poetry install
```

## Commands

```bash
# Add/remove dependency
poetry add <package>
poetry remove <package>

# Display current version
poetry version

# Build
poetry build

# List other convenient tasks
#   Always prefix these tasks with `poetry run inv` or with `inv` if in poetry shell
#   Add `--help` to get task details.
$ poetry run inv --list
Available tasks:

  clean           Run all clean sub-tasks.
  clean-build     Clean up files from package building.
  clean-docs      Clean up files from documentation builds.
  clean-python    Clean up python file artifacts.
  clean-tests     Clean up files from testing.
  commit          Run commitizen.
  commit-check    Check commit messages format.
  format          Format code.
  hooks           Run pre-commit hooks.
  install         Run all install sub-tasks.
  install-hooks   Install pre-commit hooks.
  lint            Run flake8.
  release         Release package: bump version, generate changelog and set git tag.
  test            Run tests.
```

## Process

Our process relies on [conventional commit](https://www.conventionalcommits.org/en/v1.0.0/) and [semantic versioning](https://semver.org/) standards.

To enforce their application, we rely on following tools:

- [Poetry](https://python-poetry.org/docs) for package management
- [Commitizen](https://commitizen-tools.github.io/commitizen) for commit message formatting and check
- [Python semantic release](https://python-semantic-release.readthedocs.io/en/latest/) for version management, changelog generation

## Maintainers

### Gitlab CI/CD

Required CI/CD variables:

| Key                 | Protected | Masked  | Description                                                                       |
| ------------------- | --------- | ------- | --------------------------------------------------------------------------------- |
| CI_USER_EMAIL       | X         |         | Gitlab repository maintainer user email for this project (to push release commit) |
| CI_USER_NAME        | X         |         | Gitlab repository maintainer user name for this project (to push release commit)  |
| GL_TOKEN            | X         | X       | Gitlab repository write-access token (to push release commit)                     |
| DEPLOY_REGISTRY_URL |           |         | Python package registry URL (to publish package)                                  |
| DEPLOY_TOKEN        | X         | X       | Python package registry password/token (to publish package)                       |
| DEPLOY_TOKEN_USER   | X         | X       | Python package registry user (to publish package)                                 |

Required protected branches/tag:

| Branch/Tag | Allow to merge | Allow to push |
| ---------- | -------------- | ------------- |
| master     | maintainer     | maintainer    |
| v\*        | maintainer     | maintainer    |
