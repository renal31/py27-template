# Python 2.7 template

This template provide a modern development environment for Python 2.7...

Why doing so ???

I have to maintain old python projects and wanted to do it with modern tooling and processes. :)

This template provides:
- poetry dependency management
- linting & code style (flake8, black, ...)
- test (nose2 as unittest is provided as part of python 2.7)
- conventional commit helper and checker
- pre-commit hooks to enforce qa earlier as possible
- semantic release made easy
- gitlab CI

> This template was inspired by [Cookiecutter Modern PyPackage](https://github.com/fedejaure/cookiecutter-modern-pypackage)

## Install template

```bash
pip install cookiecutter
cookiecutter https://gitlab.com/renal31/py27-template.git
```

Then, read `CONTRIBUTING.md` in the freshly installed project to get started !
